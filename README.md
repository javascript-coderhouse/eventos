# Curso JavaScript Coderhouse

## Clase 09 - Eventos

### Incorporar eventos

---

**Tipo de desafío:** 
Desafío entregable.

**Formato:** 
Página HTML y código fuente en JavaScript. Debe identificar el apellido del estudiante en el nombre de archivo comprimido por “claseApellido”. 

**Sugerencia:**
Es posible asociar más de un evento a un elemento y se pueden emplear función comunes, anónimas y arrow para los manejadores de eventos.

---

**>> Consigna:**
Con lo que vimos sobre DOM, ahora puedes sumarlo a tu proyecto, para interactuar entre los elementos HTML y JS. Es decir, asociar eventos que buscamos controlar sobre los elementos de la interfaz de nuestro simulador.


**>> Aspectos a incluir en el entregable:**
Archivo HTML y Archivo JS, referenciado en el HTML por etiqueta <script src="js/miarchivo.js"></script>, que incluya la definición de un algoritmo en JavaScript que opere sobre el DOM manejando eventos.


**>> Ejemplo:**

- Cuando el usuario completa algún dato, por ejemplo cantidad de cuotas, se captura ese dato y se agregan elementos al DOM mediante JS.

- Capturar la tecla ENTER para confirmar alguna acción.

---

### Descripción de la tarea desarrollada

